
#ifndef PAGING_H
#define PAGING_H

#include "types.h"
#include "lib.h"

void init_paging();		
void paging_asm_init(); 
int32_t proc_remap(int pid);
void vid_remap(int term_id);

#endif 
