#include "paging.h"
//#include "terminal.h"
//#include "process.h"
#define entry_num 						1024				
#define page_size   					4096				
#define kernel 							0x400000
#define shell							0x800000
#define kernel_page						1
#define user_prog						32
#define vid_mem							33
#define video_mem_start					0xB8
			
#define bit_1							0x2					
#define bit_01							0x3	
#define bit_012							0x7					
#define bit_017							0x83			
#define bit_0127						0x87


#define term0_vid						0xB9
#define term1_vid						0xBA
#define term2_vid						0xBB
#define _4mb 							0x400000
#define proc_max						6
static uint32_t page_directory_entry[entry_num] __attribute__((aligned(page_size)));
static uint32_t page_table_entry[entry_num] __attribute__((aligned(page_size)));
static uint32_t vid_page_table_entry[entry_num] __attribute__((aligned(page_size)));

/*interface:  
 * initialize the paging with different terminals
 */	
void init_paging(){
	int index;
	for (index = 0; index < entry_num; index++){	/* unpresent */	
		page_directory_entry[index] = bit_1;	
		page_table_entry[index] = (index * page_size) | bit_1;	
	}
	page_directory_entry[0] = ((unsigned)page_table_entry) | bit_01; /* video memory */  
	page_table_entry[video_mem_start] |= bit_01; 
	page_table_entry[term0_vid] |= bit_01;
	page_table_entry[term1_vid] |= bit_01;
	page_table_entry[term2_vid] |= bit_01;
	page_directory_entry[kernel_page] = kernel | bit_017;/* kernel 4mb page */
	paging_asm_init();						
}	

/* interface:
 * 
 * gives memory for the new process 
 */	
int32_t proc_remap(int pid){
	
	if (pid < 0 || pid >= proc_max){ 
		return -1;
	}
	page_directory_entry[user_prog] = (shell + _4mb * pid) | bit_0127;
	asm volatile(
		"movl %0, %%eax\n"				/*now flush TLB */
		"movl %%eax, %%cr3\n"
		:
		: "r" (page_directory_entry)
		: "eax", "memory", "cc"
	);
	return 0;
}	

/* vid_remap
 * Input: terminal id
 * Output: none  
 * function: map the video memory for process run 
 * in current terminal, dump memory for process run
 * in inactive terminal
 */	
void vid_remap(int term_id){
	page_directory_entry[vid_mem] = ((uint32_t)vid_page_table_entry) | bit_012;	
	//vid_page_table_entry[term_id] = vid_addr(term_id) | bit_012; /* vid_page_table entry 0, 1, 2 are used by terminal 0, 1, 2 seperately */	
	asm volatile(					/* now flush TLB */
		"movl %0, %%eax\n"
		"movl %%eax, %%cr3\n"
		:
		: "r" (page_directory_entry)
		: "eax"
	);
}
	
/* paging_asm_init
 * Input: none
 * Output: none
 * side effect: set Paging flag at cr0 bit 31
 * set page size extension flag at 	cr4 bit 4
 * set cr3 as base address of page directory at 0 
 */
void paging_asm_init(){
	asm volatile(   			    
		"movl %0, %%eax;"		  /* page directory base address is saved in cr3 */   
		"movl %%eax, %%cr3;"	
		: 
		: "r" (page_directory_entry)	
		: "eax","memory", "cc"	
	);

	asm volatile (		
		"movl %%cr4, %%eax;" 	  		
		"orl $0x00000010, %%eax;" /* second bit set to 1 which enables the PSE */
		"movl %%eax, %%cr4;"
		"movl %%cr0, %%eax;"			
		"orl $0x80000000, %%eax;" /* set paging flag(PG) */
		"movl %%eax, %%cr0;"											
		: 						
		: 	
		: "eax", "memory", "cc" 
	);		
}
