#include "keyboard.h"

struct keyboard{
    char key;
    char value; 
    char press_shift;
};

struct keyboard keyboard_scancode[119];

void init_keyboard_table(struct keyboard* keyboard_scancode){
    keyboard_scancode[0].key = 0x01; //esc
    keyboard_scancode[0].value = 0x1B;

    keyboard_scancode[1].key = 0x02; //1
    keyboard_scancode[1].value = 0x31;
    keyboard_scancode[1].press_shift = 0x21;//!

    keyboard_scancode[2].key = 0x03; //2 
    keyboard_scancode[2].value = 0x32;
    keyboard_scancode[2].press_shift = 0x40;//@

    keyboard_scancode[3].key = 0x04; //3 
    keyboard_scancode[3].value = 0x33;
    keyboard_scancode[3].press_shift = 0x23;//#

    keyboard_scancode[4].key = 0x05; //4
    keyboard_scancode[4].value = 0x34;
    keyboard_scancode[4].press_shift = 0x24;//$

    keyboard_scancode[5].key = 0x06; //5
    keyboard_scancode[5].value = 0x35;
    keyboard_scancode[5].press_shift = 0x25;//%

    keyboard_scancode[6].key = 0x07; //6
    keyboard_scancode[6].value = 0x36;
    keyboard_scancode[6].press_shift = 0x5E;//^

    keyboard_scancode[7].key = 0x08; //7
    keyboard_scancode[7].value = 0x37;
    keyboard_scancode[7].press_shift = 0x26;//&

    keyboard_scancode[8].key = 0x09; //8
    keyboard_scancode[8].value = 0x38;
    keyboard_scancode[8].press_shift = 0x2A;//*

    keyboard_scancode[9].key = 0x0A; //9
    keyboard_scancode[9].value = 0x39;
    keyboard_scancode[9].press_shift = 0x28;//(

    keyboard_scancode[10].key = 0x0B;//0
    keyboard_scancode[10].value = 0x30;
    keyboard_scancode[10].press_shift = 0x29;//)
    
    keyboard_scancode[11].key = 0x81;//esc released  
    keyboard_scancode[12].key = 0x82;//1 released 
    keyboard_scancode[13].key = 0x83;//2 released 
    keyboard_scancode[14].key = 0x84;//3 released 
    keyboard_scancode[15].key = 0x85;//4 released 
    keyboard_scancode[16].key = 0x86;//5 released 
    keyboard_scancode[17].key = 0x87;//6 released 
    keyboard_scancode[18].key = 0x88;//7 released 
    keyboard_scancode[19].key = 0x89;//8 released 
    keyboard_scancode[20].key = 0x8A;//9 released 
    keyboard_scancode[21].key = 0x8B;//0 released 
    
    //alphabets
    keyboard_scancode[22].key = 0x10;//q
    keyboard_scancode[22].value = 0x71;
    keyboard_scancode[22].press_shift = 0x51;//Q
    keyboard_scancode[23].key = 0x11;//w
    keyboard_scancode[23].value = 0x77;
    keyboard_scancode[23].press_shift = 0x57;//W

    keyboard_scancode[24].key = 0x12;//e
    keyboard_scancode[24].value = 0x65;
    keyboard_scancode[24].press_shift = 0x45;//E

    keyboard_scancode[25].key = 0x13;//r
    keyboard_scancode[25].value = 0x72;
    keyboard_scancode[25].press_shift = 0x52;//R

    keyboard_scancode[26].key = 0x14;//t
    keyboard_scancode[26].value = 0x74;
    keyboard_scancode[26].press_shift = 0x54;//T

    keyboard_scancode[27].key = 0x15;//y
    keyboard_scancode[27].value = 0x79;
    keyboard_scancode[27].press_shift = 0x59;//Y

    keyboard_scancode[28].key = 0x16;//u
    keyboard_scancode[28].value = 0x75;
    keyboard_scancode[28].press_shift = 0x55;//U

    keyboard_scancode[29].key = 0x17;//i 
    keyboard_scancode[29].value = 0x69;
    keyboard_scancode[29].press_shift = 0x49;//I

    keyboard_scancode[30].key = 0x18;//o 
    keyboard_scancode[30].value = 0x6F;
    keyboard_scancode[30].press_shift = 0x4F;//O

    keyboard_scancode[31].key = 0x19;//p
    keyboard_scancode[31].value = 0x70;
    keyboard_scancode[31].press_shift = 0x50;//P

    keyboard_scancode[32].key = 0x1E;//a
    keyboard_scancode[32].value = 0x61;
    keyboard_scancode[32].press_shift = 0x41;//A

    keyboard_scancode[33].key = 0x1F;//s 
    keyboard_scancode[33].value = 0x73;
    keyboard_scancode[33].press_shift = 0x53;//S

    keyboard_scancode[34].key = 0x20;//d
    keyboard_scancode[34].value = 0x64;
    keyboard_scancode[34].press_shift = 0x44;//D

    keyboard_scancode[35].key = 0x21;//f
    keyboard_scancode[35].value = 0x66;
    keyboard_scancode[35].press_shift = 0x46;//F

    keyboard_scancode[36].key = 0x22;//g
    keyboard_scancode[36].value = 0x67;
    keyboard_scancode[36].press_shift = 0x47;//G

    keyboard_scancode[37].key = 0x23;//h
    keyboard_scancode[37].value = 0x68;
    keyboard_scancode[37].press_shift = 0x48;//H

    keyboard_scancode[38].key = 0x24;//j
    keyboard_scancode[38].value = 0x6A;
    keyboard_scancode[38].press_shift = 0x4A;//J

    keyboard_scancode[39].key = 0x25;//k
    keyboard_scancode[39].value = 0x6B;
    keyboard_scancode[39].press_shift = 0x4B;//K

    keyboard_scancode[40].key = 0x26;//l
    keyboard_scancode[40].value = 0x6C;
    keyboard_scancode[40].press_shift = 0x4C;//L

    keyboard_scancode[41].key = 0x2C;//z
    keyboard_scancode[41].value = 0x7A;
    keyboard_scancode[41].press_shift = 0x5A;//Z

    keyboard_scancode[42].key = 0x2D;//x 
    keyboard_scancode[42].value = 0x78;
    keyboard_scancode[42].press_shift = 0x58;//X

    keyboard_scancode[43].key = 0x2E;//c
    keyboard_scancode[43].value = 0x63;
    keyboard_scancode[43].press_shift = 0x43;//C

    keyboard_scancode[44].key = 0x2F;//v 
    keyboard_scancode[44].value = 0x76;
    keyboard_scancode[44].press_shift = 0x56;//V

    keyboard_scancode[45].key = 0x30;//b
    keyboard_scancode[45].value = 0x62;
    keyboard_scancode[45].press_shift = 0x42;//B

    keyboard_scancode[46].key = 0x31;//n
    keyboard_scancode[46].value = 0x6E; 
    keyboard_scancode[46].press_shift = 0x4E;//N

    keyboard_scancode[47].key = 0x32;//m
    keyboard_scancode[47].value = 0x6D;
    keyboard_scancode[47].press_shift = 0x4D;//M
    
    keyboard_scancode[48].key = 0x90;//q released 
    keyboard_scancode[49].key = 0x91;//w released 
    keyboard_scancode[50].key = 0x92;//e released 
    keyboard_scancode[51].key = 0x93;//r released 
    keyboard_scancode[52].key = 0x94;//t released 
    keyboard_scancode[53].key = 0x95;//y released 
    keyboard_scancode[54].key = 0x96;//u released 
    keyboard_scancode[55].key = 0x97;//i released 
    keyboard_scancode[56].key = 0x98;//o released 
    keyboard_scancode[57].key = 0x99;//p released 
    keyboard_scancode[58].key = 0x9E;//a released 
    keyboard_scancode[59].key = 0x9F;//s released
    keyboard_scancode[60].key = 0xA0;//d released 
    keyboard_scancode[61].key = 0xA1;//f released 
    keyboard_scancode[62].key = 0xA2;//g released 
    keyboard_scancode[63].key = 0xA3;//h released 
    keyboard_scancode[64].key = 0xA4;//j released 
    keyboard_scancode[65].key = 0xA5;//k released 
    keyboard_scancode[66].key = 0xA6;//l released 
    keyboard_scancode[67].key = 0xAC;//z released 
    keyboard_scancode[68].key = 0xAD;//x released 
    keyboard_scancode[69].key = 0xAE;//c released 
    keyboard_scancode[70].key = 0xAF;//v released 
    keyboard_scancode[71].key = 0xB0;//b released 
    keyboard_scancode[72].key = 0xB1;//n released 
    keyboard_scancode[73].key = 0xB2;//m released 
    
    
    /* left shift press & release */
    keyboard_scancode[74].key = 0x2A;//l shift 
    keyboard_scancode[75].key = 0xAA;//l shift released
    /* right shift press & release */
    keyboard_scancode[76].key = 0x36;//r shift 
    keyboard_scancode[77].key = 0xB6;//r shift released
    /* capslock press & release */
    keyboard_scancode[78].key = 0x3A;//capslock 
    keyboard_scancode[79].key = 0xBA;//capslock released
    
    /* backspace press */
    keyboard_scancode[80].key = 0x0E;//backspace 
    
    /* enter press */
    keyboard_scancode[81].key = 0x1C;//enter 
    keyboard_scancode[81].value = 0x0A; //'\n'
    keyboard_scancode[81].press_shift = 0x0A;//'\n'
    /* other char */
    keyboard_scancode[82].key = 0x0C;//- 
    keyboard_scancode[82].value = 0x2D;//-
    keyboard_scancode[82].press_shift = 0x5F;//_
    keyboard_scancode[83].key = 0x0D;//= 
    keyboard_scancode[83].value = 0x3D;//=
    keyboard_scancode[83].press_shift = 0x2B;//+
    keyboard_scancode[84].key = 0x1A;//[ 
    keyboard_scancode[84].value = 0x5B;//[
    keyboard_scancode[84].press_shift = 0x7B;//{
    keyboard_scancode[85].key = 0x1B;//] 
    keyboard_scancode[85].value = 0x5D;//]
    keyboard_scancode[85].press_shift = 0x7D;//}
    keyboard_scancode[86].key = 0x27;//; 
    keyboard_scancode[86].value = 0x3B;//;
    keyboard_scancode[86].press_shift = 0x3A;//:   
    keyboard_scancode[87].key = 0x28;//'  
    keyboard_scancode[87].value = 0x27;//'
    keyboard_scancode[87].press_shift = 0x22;//"
    keyboard_scancode[88].key = 0x29;//` 
    keyboard_scancode[88].value = 0x60;//`
    keyboard_scancode[88].press_shift = 0x7E;//~
    keyboard_scancode[89].key = 0x2B;//\ 
    keyboard_scancode[89].value = 0x5C;//'\' 
    keyboard_scancode[89].press_shift = 0x7C;//|   
    keyboard_scancode[90].key = 0x33;//, 
    keyboard_scancode[90].value = 0x2C;//,
    keyboard_scancode[90].press_shift = 0x3C;//<
    keyboard_scancode[91].key = 0x34;//. 
    keyboard_scancode[91].value = 0x2E;//.
    keyboard_scancode[91].press_shift = 0x3E;//>   
    keyboard_scancode[92].key = 0x35;//"/" 
    keyboard_scancode[92].value = 0x2F;// "/"
    keyboard_scancode[92].press_shift = 0x3F;// ?  
    keyboard_scancode[93].key = 0x39;//space 
    keyboard_scancode[93].value = 0x20;//space
    keyboard_scancode[93].press_shift = 0x20;//space
    
    keyboard_scancode[94].key = 0x8C;//- release
    keyboard_scancode[95].key = 0x8D;//= release
    keyboard_scancode[96].key = 0x8E;//backsapce release
    keyboard_scancode[97].key = 0x9A;//[ release
    keyboard_scancode[98].key = 0x9B;//] release
    keyboard_scancode[99].key = 0x9C;//enter release
    keyboard_scancode[100].key = 0xA8;//' release
    keyboard_scancode[101].key = 0xA9;//` release
    keyboard_scancode[102].key = 0xAB;//\ release
    keyboard_scancode[103].key = 0xB3;//, release
    keyboard_scancode[104].key = 0xB4;//. release
    keyboard_scancode[105].key = 0xB5;//'/' release
    keyboard_scancode[106].key = 0xA7;//; release
    
    keyboard_scancode[107].key = 0x9C;//enter release
    keyboard_scancode[108].key = 0xB9;//space release
    keyboard_scancode[109].key = 0x8E;//backspace released
    
    /* left control press & realease */
    keyboard_scancode[110].key = 0x1D;//r control pressing
    keyboard_scancode[111].key = 0x9D;//l control released
    
    /* left alt press & release */
    keyboard_scancode[112].key = 0x38;//left control pressing
    keyboard_scancode[113].key = 0xB8;//left alt released
    
    /* F1 F2 F3 */
    keyboard_scancode[114].key = 0x3B;//F1 pressing
    keyboard_scancode[115].key = 0xBB;//F1 released
    
    keyboard_scancode[116].key = 0x3C;//F2 pressing
    keyboard_scancode[117].key = 0xBC;//F2 released
    
    keyboard_scancode[118].key = 0x3D;//F3 pressing
    keyboard_scancode[119].key = 0xBD;//F3 released
    
}

void init_keyboard(){
    init_keyboard_table(keyboard_scancode);
    enable_irq(0x01);
}
