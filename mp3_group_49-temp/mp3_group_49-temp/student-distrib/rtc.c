#include "rtc.h"

void init_rtc(){
    unsigned char copy; 
    char rtc_rate;

    copy = inb(0x71); 
    outb(0x8B, 0x70);
    outb((copy | 0x40), 0x71);
    rtc_rate = 0x0F;
    outb(0x8A, 0x70);
    copy = inb(0x71);
    outb(0x8A, 0x70);
    outb(((copy & 0xF0) | 0x0F), 0x71);
    enable_irq(0x02);
    enable_irq(0x08);
}
