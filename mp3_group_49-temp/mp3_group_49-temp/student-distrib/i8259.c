/* i8259.c - Functions to interact with the 8259 interrupt controller
 * vim:ts=4 noexpandtab
 */

#include "i8259.h"
#include "lib.h"

/* Interrupt masks to determine which interrupts are enabled and disabled */
uint8_t master_mask; /* IRQs 0-7  */
uint8_t slave_mask;  /* IRQs 8-15 */

/* Initialize the 8259 PIC */
void i8259_init(void) {
	//mask all interrupts
	outb(0xFF, 0x21);
	outb(0xFF, 0xA1);

	//initialize PICs
	outb(0x11, 0x20);//master PIC
	outb(0x20, 0x21);
	outb(0x04, 0x21);

	outb(0x11, 0xA0);//slave PIC
	outb(0x28, 0xA1);
	outb(0x04, 0xA1);
	outb(0x01, 0xA1);
}

/* Enable (unmask) the specified IRQ */
void enable_irq(uint32_t irq_num) {
	unsigned int enable_irq;
	enable_irq=0;
	if(irq_num<0) //invalid irq_num
		return;
	else if(irq_num<=7){ //master's irq
		enable_irq=~(1<<irq_num);
		master_mask = (inb(0x21) & enable_irq); //get the IRQof master
		outb(master_mask, 0x21); //enable the irq
	}
	else if(irq_num>7){ //slave's irq
		irq_num=irq_num-8; //get the slave's IRQ
		enable_irq=~(1<<irq_num); 
		slave_mask = (inb(0xA1) & enable_irq); //get the IRQ of slave
		outb(master_mask, 0xA1); //enable the irq
	}
}

/* Disable (mask) the specified IRQ */
void disable_irq(uint32_t irq_num) {
	unsigned int mask_irq;
	mask_irq=0;
	if(irq_num<0) //invalid irq_num
		return;
	else if(irq_num<=7){ //master's irq
		mask_irq=1<<irq_num;
		master_mask = (inb(0x21) | mask_irq); //get the IRQof master
		outb(master_mask, 0x21); //mask the irq
	}
	else if(irq_num>7){ //slave's irq
		irq_num=irq_num-8; //get the slave's IRQ
		mask_irq=1<<irq_num; 
		slave_mask = (inb(0xA1) | mask_irq); //get the IRQ of slave
		outb(master_mask, 0xA1); //mask the irq
	}
}

/* Send end-of-interrupt signal for the specified IRQ */
void send_eoi(uint32_t irq_num) {
	if(irq_num<0) //invalid irq_num
		return;
	if (irq_num <= 7){ //master chip
		outb((0x60+irq_num), 0x20); //tell the master
	}
	else{ //slave chip
		outb((0x60 + (irq_num & 7)), 0xA0); //tell the slave chip
		outb(0x62, 0x20); //tell the master chip
	}
}
